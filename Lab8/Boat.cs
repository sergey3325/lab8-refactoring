﻿using System;
using System.Drawing;


namespace Lab8
{
class Boat : GameObject
{
	static Bitmap spriteTableBitmap = new Bitmap("SpriteTable.png");
	static Sprite spriteTable = new Sprite(spriteTableBitmap, 1, 9);

	enum MovingDirection
	{
		Left,
		Right
	}

	MovingDirection dir;

	public Boat(Bitmap staticImage, Point location) : base(staticImage, new Rectangle(location, spriteTable.OneElementSize))
	{
		Random r = new Random();
		if(r.Next() % 2 == 0)
			dir = MovingDirection.Right;
		else
			dir = MovingDirection.Left;
	}

	public override void Move()
	{
		Move(5);
	}

	public void Move(int step)
	{
		switch(dir)
		{
			case MovingDirection.Left:
				MoveLeft(step);
				spriteTable.OneStepLeft();
				break;
			case MovingDirection.Right:
				MoveRight(step);
				spriteTable.OneStepRight();
				break;
		}
	}

	private void MoveRight(int step)
	{
		region.X += step;
		if(region.X > sceneSize.Width - region.Width)
		{
			region.X = sceneSize.Width - region.Width;
			dir = MovingDirection.Left;
		}
	}

	private void MoveLeft(int step)
	{
		region.X -= step;
		if(region.X <= 0)
		{
			region.X = 0;
			dir = MovingDirection.Right;
		}
	}

	new public void Draw()
	{
		Draw(dir == MovingDirection.Right ? 
					spriteTable.SourceRectangle : Neg(spriteTable.SourceRectangle)
			);
	}

	protected override void InitSprite()
	{
		sprite = spriteTable.Image;
	}

	private Rectangle Neg(Rectangle a)
	{
		Rectangle negativeA = a;
		negativeA.X += a.Width;
		negativeA.Width *= -1;
		return negativeA;
	}
}
}
