﻿using System.Drawing;
using System.Drawing.Drawing2D;

namespace Lab8
{
class Cannon : GameObject
{
	const int Width = 120, Height = Width;
	Bitmap cannonBmp;
	int angle;

	public Cannon(Bitmap staticImage) : base(staticImage, new Rectangle((staticImage.Width - Width) / 2, staticImage.Height - Height, Width, Height))
	{
		cannonBmp = new Bitmap(Width, Height);

	}

	public void Rotate(int angle)
	{
		this.angle = -angle;
		InitSprite();
	}

	public override void Move()
	{
			
	}

	private int f(int Width, int k)
	{
		return Width / 4 + (Width / 2) / 3 * k;
	}

	protected override void InitSprite()
	{
		Graphics spriteGraphics = Graphics.FromImage(sprite);

		Matrix rotateTransform = new Matrix();
		rotateTransform.RotateAt(angle, new Point(Width / 2, Height / 2));
		spriteGraphics.Transform = rotateTransform;
		rotateTransform.Dispose();
		spriteGraphics.Clear(Color.Transparent);

		Point[] cannonPolygon = new Point[] {
			new Point(f(Width , 0), Height/4 * 3),
			new Point(f(Width , 1), Height/4),
			new Point(f(Width , 2), Height/4),
			new Point(f(Width , 3), Height/4 * 3)
		};
		spriteGraphics.DrawPolygon(Pens.Black, cannonPolygon);
		spriteGraphics.FillPolygon(Brushes.DarkGray, cannonPolygon);
		spriteGraphics.Dispose();
	}
}
}
