﻿using System;
using System.Drawing;

namespace Lab8
{
class Cannonball : GameObject
{
	int angle;
	const int Width = 4;
	const int Height = 4;

	public Cannonball(Bitmap staticImage, Point centerLocation, int angle) : base(staticImage, new Rectangle(centerLocation.X - Width / 2, centerLocation.Y - Height/ 2, Width, Height))
	{
		this.angle = angle;
	}

	~Cannonball()
	{

	}

	public override void Move()
	{
		int dx = (int)(Math.Sin(-angle / 180.0 * Math.PI) * 10);
		int dy = (int)(Math.Cos(-angle / 180.0 * Math.PI) * 10);
		region.X += dx;
		region.Y -= dy;
	}

	protected override void InitSprite()
	{
		Graphics spriteGraphics = Graphics.FromImage(sprite);
		spriteGraphics.FillEllipse(Brushes.Black, 0, 0, region.Width, region.Height);
		spriteGraphics.Dispose();
	}
}
}
