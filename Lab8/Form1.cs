﻿using System;
using System.Windows.Forms;

namespace Lab8
{
	public partial class Form1 : Form
	{

		Game game;

		public Form1()
		{
			InitializeComponent();
			pictureBox1.MouseMove += PictureBox1_MouseMove;
			pictureBox1.MouseClick += PictureBox1_Click;
			game = new Game(pictureBox1, ShipsCount, RestOfShips, Scores);
			game.InitScene();
		}

		private int getAngle(PictureBox sender, int x, int y)
		{
			return (int)
				(Math.Atan((sender.Width / 2.0 - x) / (sender.Height - y) ) / Math.PI * 180);
		}

		private void PictureBox1_Click(object sender, MouseEventArgs e)
		{
			game.AddCannonball(getAngle((PictureBox)sender, e.X, e.Y));
		}

		private void PictureBox1_MouseMove(object sender, MouseEventArgs e)
		{
			game.RotateCannon(getAngle((PictureBox)sender, e.X, e.Y));
		}

		private void button1_Click(object sender, EventArgs e)
		{
			game.AddBoat();
			game.PauseOrResume();
		}
	}
}
