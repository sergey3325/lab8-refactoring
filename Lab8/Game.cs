﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace Lab8
{
	class Game
	{
		Random rand = new Random();
		bool Pause = false;
		Bitmap backImage = new Bitmap("Back.png");

		Timer timer;

		LinkedList<Boat> boats = new LinkedList<Boat>();
		LinkedList<Cannonball> cannonballs = new LinkedList<Cannonball>();
		Cannon cannon;

		double step = 2;

		Label shipsLabel;
		Label restOfShipsLabel;
		Label scoresLabel;
		int ships = 0;
		int rest = 0;
		int scores = 0;
		PictureBox canvas;

		private void PrintInfo()
		{
			shipsLabel.Text = "" + ships;
			restOfShipsLabel.Text = "" + rest;
			scoresLabel.Text = "" + scores;
		}

		public Game(PictureBox canvas, Label ships, Label restOfShips, Label scores)
		{
			shipsLabel = ships;
			restOfShipsLabel = restOfShips;
			scoresLabel = scores;
			PrintInfo();
			canvas.Image = backImage;
			this.canvas = canvas;
		}

		public void InitScene()
		{
			cannon = new Cannon(backImage);
			timer = new Timer();
			timer.Interval = 75;
			timer.Tick += timerTick;
		}
		
		public void PauseOrResume()
		{
			Pause = !Pause;
		}

		public void AddBoat()
		{
			timer.Start();
			boats.AddLast(new Boat(backImage, new Point(rand.Next(0, backImage.Width - 50), 200)));
			++ships;
			++rest;
		}

		private double ConvertDegToRad(double angle)
		{
			return angle / 180.0 * Math.PI;
		} 

		public void AddCannonball(int angle)
		{
			Point c = new Point(cannon.Region.X + cannon.Region.Width / 2
				, cannon.Region.Y + cannon.Region.Height / 2);
			int dx = (int)(Math.Sin(ConvertDegToRad(-angle))
				* cannon.Region.Width / Math.Sqrt(2));
			int dy = (int)(Math.Cos(ConvertDegToRad(-angle))
				* cannon.Region.Height / Math.Sqrt(2));
			c.X += dx;
			c.Y -= dy;
			cannonballs.AddLast(new Cannonball(backImage, c, angle));
		}

		public void RotateCannon(int angle)
		{
			cannon.Rotate(angle);
		}

		private void SaveAll()
		{
			foreach(Cannonball ball in cannonballs)
			{
				ball.SaveStaticImageState();
			}
			foreach(Boat boat in boats)
			{
				boat.SaveStaticImageState();
			}
			cannon.SaveStaticImageState();
		}

		private void RestoreAll()
		{
			foreach(Cannonball ball in cannonballs)
			{
				ball.RestoreSavedPart();
			}
			foreach(Boat boat in boats)
			{
				boat.RestoreSavedPart();
			}
			cannon.RestoreSavedPart();
		}

		private bool IsOutOfGamefield(GameObject obj)
		{
			return (obj.Region.X < -obj.Region.Width || obj.Region.X > canvas.Width)
					&& (obj.Region.Y < -obj.Region.Height || obj.Region.Y > canvas.Height);
		}

		private void MoveAll()
		{
			step += 0.01;

			for(LinkedListNode<Cannonball> ball = cannonballs.First
				; ball != null
				; ball = ball.Next)
			{
				ball.Value.Move();

				if(IsOutOfGamefield(ball.Value))
					cannonballs.Remove(ball);

				for(LinkedListNode<Boat> boat = boats.First
					; boat != null
					; boat = boat.Next)
				{
					if(ball.Value.Region.IntersectsWith(boat.Value.Region))
					{
						boats.Remove(boat);
						cannonballs.Remove(ball);
						--rest;
						scores += 100;
						break;
					}
				}
			}

			foreach(Boat boat in boats)
			{
				boat.Move((int)step);
			}
		}

		private void DrawAll()
		{
			foreach(Cannonball ball in cannonballs)
			{
				ball.Draw();
			}
			foreach(Boat boat in boats)
			{
				boat.Draw();
			}
			cannon.Draw();

		}

		private void timerTick(object sender, EventArgs e)
		{
			RestoreAll();
			MoveAll();
			SaveAll();
			DrawAll();
			canvas.Invalidate();
			PrintInfo();
		}
	}
}
