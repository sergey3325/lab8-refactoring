﻿using System.Drawing;

namespace Lab8
{
abstract class GameObject
{
		
	public Rectangle Region { get { return region; } }

	public GameObject(Bitmap staticImage, Rectangle region)
	{
		this.staticImage = staticImage;
		sceneSize = staticImage.Size;
		this.region = region;

		g = Graphics.FromImage(staticImage);
		sprite = new Bitmap(region.Width, region.Height);
			
		InitSprite();
	}

	~GameObject()
	{

	}

	abstract public void Move();

	public void RestoreSavedPart()
	{
		if(savedPart != null)
			g.DrawImage(savedPart, savedPartRegion);
	}

	public void Draw()
	{
		g.DrawImage(sprite, region);
	}

	public void Draw(Rectangle spriteSourceRectangle)
	{
		g.DrawImage(sprite, region, spriteSourceRectangle, GraphicsUnit.Pixel);
	}

	public void SaveStaticImageState()
	{
		savedPartRegion = region;
		if(region.Y >= 0 
			&& region.X >= 0 
			&& region.X + region.Width <= staticImage.Width 
			&& region.Y + region.Height <= staticImage.Height)
		{
			savedPart = staticImage.Clone(savedPartRegion, staticImage.PixelFormat);
		}
	}


	abstract protected void InitSprite();

	protected Rectangle region;
	protected Graphics g;
	protected Bitmap sprite;
	protected Size sceneSize;

	private Bitmap staticImage;
	private Bitmap savedPart;
	private Rectangle savedPartRegion;
}
}
