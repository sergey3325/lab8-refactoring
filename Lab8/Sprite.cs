﻿using System.Drawing;

namespace Lab8
{
class Sprite
{
	Bitmap spriteTable;
	Size spriteSize;
	Point spriteLocation;

	public Bitmap Image { get { return spriteTable; } }
	public Rectangle SourceRectangle { get { return new Rectangle(spriteLocation, spriteSize); } }
	public Size OneElementSize { get { return spriteSize; } }

	public Sprite(Bitmap spriteTable, int rows = 1, int columns = 1)
	{
		this.spriteTable = spriteTable;
		spriteSize = new Size(spriteTable.Width / columns, spriteTable.Height / rows);
		spriteLocation = new Point(0, 0);
	}

	public void OneStepRight()
	{
		spriteLocation.X += spriteSize.Width;
		if(spriteLocation.X >= spriteTable.Width)
			spriteLocation.X = 0;
	}

	public void OneStepLeft()
	{
		if(spriteLocation.X == 0)
			spriteLocation.X = spriteTable.Width;
		spriteLocation.X -= spriteSize.Width;
	}

	public void OneStepUp()
	{
		if(spriteLocation.Y == 0)
			spriteLocation.Y = spriteTable.Height;
		spriteLocation.Y -= spriteSize.Height;
	}

	public void OneStepDown()
	{
		spriteLocation.Y += spriteSize.Height;
		if(spriteLocation.Y >= spriteTable.Height)
			spriteLocation.Y = 0;
	}

}
}
